# Descrizione

Un progetto Java Springboot semplice e snello per le POC di CI/CD


# Chiamate

## GET Glossary

```curl
curl --location --request GET 'http://localhost:8080/glossary/1.0.0/?with_description=true'
```

## OpenApi

```curl
curl --location --request GET 'http://localhost:8080/glossary/doc/v3/api-docs'
```

## Swagger-UI

```curl
curl --location --request GET 'http://localhost:8080/glossary/doc/webjars/swagger-ui/index.html'
```