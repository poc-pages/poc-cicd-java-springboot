package it.notartel.poc.model;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import it.notartel.poc.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
/* Any property not bound in this type should be ignored. */
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
public class Glossary implements Serializable {
	
	/** serialVersionUID. */
	private static final long serialVersionUID = -3498897674689741202L;
	
	/* Cross function data */
	@NotNull
	private List<GlossaryItem> list;

	/** Return a list of GlossaryItems with only the name.
	 * 
	 * @return
	 */
	public List<GlossaryItem> getNameList() {
		return list.stream()
					.map(item -> new GlossaryItem(item.name, null))
					.collect(Collectors.toList());
	}
	

	@Getter
	@Setter
	@ToString
	/* Any property not bound in this type should be ignored. */
	@JsonIgnoreProperties(ignoreUnknown = true)
	@NoArgsConstructor
	public static class GlossaryItem implements Serializable {
		
		/** serialVersionUID. */
		private static final long serialVersionUID = 46142556400321862L;
	
		/* Cross function data */
		@NotNull
		private String name;
		
		@NotNull
		private String value;
		
		
		/** Constructor.
		 * 
		 * @param name
		 * @param value
		 */
		public GlossaryItem(@NotNull String name, @NotNull String value) {
			this.name = name;
			this.value = value;
			this.validate();
		}
	
		public void validate() {
			String alphaNumSymbRegex = "[0-9a-zA-Z_\' ()]+";
			Utils.validateStringIfNotNull(name, alphaNumSymbRegex, String.format("the name [%s] do not have the required pattern", name));
			Utils.validateStringIfNotNull(value, alphaNumSymbRegex, String.format("the value [%s] do not have the required pattern", value));
		}
	}
}
