package it.notartel.poc.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import it.notartel.poc.utils.Utils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
/* Any property not bound in this type should be ignored. */
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class Request implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = -6462726857914952950L;
	
	/* Cross function data */
	@NotNull
	private String apiVersion;
	
	@NotNull
	private boolean withDescription;
	
	@NotNull
	private String name;
	
	
	/** Constructor.
	 * 
	 * @param apiVersion
	 * @param withDescription
	 */
	public Request(@NotNull String apiVersion, @NotNull boolean withDescription) {
		this.apiVersion = apiVersion;
		this.withDescription = withDescription;
		this.validate();
	}


	public void validate() {
		// Version
		Utils.validateString(apiVersion, Utils.REGEX_API_VERSION , String.format("the API version [%s] do not have the required pattern %s", apiVersion, Utils.REGEX_API_VERSION));
		Utils.validateString(apiVersion, "1.0.0", String.format("the API version [%s] is not supported", apiVersion));
		
		// Headers
		Utils.validateStringIfNotNull(name, Utils.REGEX_ALPHANUM_WITH_SYMBOLS, String.format("the name value [%s] do not have the required pattern", name));
	}
}
