package it.notartel.poc.exception;

import it.notartel.poc.utils.Enums.EError;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/** This exception is useful to block the flow in case of errors from external actors (e.g.: HTTP calls). */
@Getter
@Setter
@ToString(callSuper = true)
public class GenericException extends Exception {

	/** serialVersionUID. */
	private static final long serialVersionUID = -8600788286618670029L;

	private final EError error;
	
	
	/** Constructor.
	 * 
	 * @param format
	 * @param e
	 */
	public GenericException(String message, Throwable cause) {
		super(message, cause);
		this.error = EError.GENERIC;
	}
	
	
	/** Constructor.
	 * 
	 * @param error
	 * @param message
	 */
	public GenericException(EError error, String message) {
		super(message);
		this.error = error;
	}
}
