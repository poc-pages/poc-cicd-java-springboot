package it.notartel.poc.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.web.server.ServerWebInputException;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Utils {

	public static final String REGEX_ALPHANUM = "[0-9a-zA-Z]+";
	public static final String REGEX_ALPHANUM_WITH_SYMBOLS = "[0-9a-zA-Z_]+";
	public static final String REGEX_API_VERSION = "^[0-9]+\\.[0-9]+\\.[0-9]+.*$";
	
	/** Validate the input <code>string</string>.
	 * 
	 * @param string the String to be validated.
	 * @param regex the pattern to be matched.
	 * @param errorMessage is the reason put in the ServerWebInputException thrown if the <code>string</string> does not fulfil the <code>regex</string>.
	 * @return 
	 */
	public static void validateStringIfNotNull(String string, String regex, String errorMessage) {
		if (string != null) {
			validateString(string, regex, errorMessage);
		}
	}

	/** Validate the input <code>string</string>.
	 * 
	 * @param string the String to be validated.
	 * @param regex the pattern to be matched.
	 * @param errorMessage is the reason put in the ServerWebInputException thrown if the <code>string</string> does not fulfil the <code>regex</string>.
	 * @return 
	 */
	public static boolean validateString(String string, String regex, String errorMessage) {
		if (!match(string, regex)) {
			throw new ServerWebInputException(errorMessage);
		}
		return true;
	}
	
	/** Checks if the input <code>string</string> matches the <code>regex</string> pattern.
	 * 
	 * @param string
	 * @param regex
	 * @return
	 */
	public static boolean match(String string, String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(string);
		return matcher.matches();
	}
}