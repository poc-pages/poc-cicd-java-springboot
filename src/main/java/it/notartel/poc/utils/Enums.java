package it.notartel.poc.utils;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Component
public class Enums {

	
	@Getter
	@AllArgsConstructor
	public enum EError {
		BAD_REQUEST	("000.400", "Bad request"),
		NOT_FOUND	("000.404", "Not found"),
		GENERIC		("000.500", "Generic error"),
		;
		private String errorCode;
		private String errorMessage;
	}
}