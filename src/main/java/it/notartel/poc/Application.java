package it.notartel.poc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@SpringBootApplication
public class Application {

	@Value("${url.doc.site.notartel}")
    private String urlDocSiteNotartel;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	

	@Bean
	public OpenAPI springShopOpenAPI() {
		String description = "Humble POC of CI/CD.";
		
		return new OpenAPI()
				.info(new Info().title("CI/CD POC")
								.description(description)
//								.termsOfService(urlDocSiteNotartel)
								.contact(new Contact()/*.name("supporto").url(urlDocSiteNotartel)*/.email("grupposviluppo@notariato.it"))
								.license(new License().name("Copyright Notartel S.p.a.").url(urlDocSiteNotartel))
								.version("1.0.0")
					)
				.externalDocs(new ExternalDocumentation()
						.description("Cerca dettagli su Google")
						.url("https://www.google.com"))
				;
	}

}
