package it.notartel.poc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import it.notartel.poc.exception.GenericException;
import it.notartel.poc.model.Glossary.GlossaryItem;
import it.notartel.poc.model.Request;
import it.notartel.poc.service.GlossaryService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
/* @RestController combines @Controller and @ResponseBody */
@RestController
@RequestMapping("/glossary/{apiVersion}")
public class GlossaryController extends AbstractController {
	
	@Autowired
	private GlossaryService service;
	
	
	@Operation(summary = "Retrieve the glossary list.", description = "<p>Retrieve the full list of glossary elements with, optionally, their description.</p>")
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<GlossaryItem>> getJson(
			@PathVariable(name = "apiVersion", required = true)@Parameter(description = "The API version", example = "1.0.0") String apiVersion,
			@RequestParam(value = "with_description", required = false, defaultValue = "false") boolean withDescription
			) throws GenericException {
		try {
			log.info(LOG_API_DATA, "GET", "Glossary List", apiVersion);
			Request request = new Request(apiVersion, withDescription);
			log.debug(LOG_REQUEST_IS, request);
			
	    	List<GlossaryItem> list = service.getList(request);
			log.info(LOG_RESPONSE_IS, list);
			return ResponseEntity.status(HttpStatus.OK).body(list);
			
		} catch (Exception e) {
			throw new GenericException(String.format(MSG_EXCEPTION, e.getClass()), e);
		}
	}
}