package it.notartel.poc.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import it.notartel.poc.exception.GenericException;
import it.notartel.poc.model.ErrorResponse;
import it.notartel.poc.utils.Enums.EError;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class AbstractController {

	protected static final String LOG_API_DATA = "{} {} [API version {}]";
	protected static final String LOG_REQUEST_IS = "Request is = [{}]";
	protected static final String LOG_RESPONSE_IS = "API ended with response = [{}]";
	protected static final String LOG_NO_RESPONSE = "API ended with no response";
	protected static final String LOG_EXCEPTION = "Execution failure: was thrown {}";
	protected static final String MSG_EXCEPTION = "Execution failure: was thrown %s";
    
    
	//HTTP 400
    /** Manage bean validation (https://beanvalidation.org/2.0/).
     * 
     * @param excemption
     * @return
     */
	@ApiResponse(responseCode = "400",
				 description = "The request is not valid.")
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse handleValidationExceptions(MethodArgumentNotValidException e) {
    	
    	log.error("Handling validation exception | {}", e);
    	Map<String, String> errorMap = new HashMap<>();
    	
    	e.getBindingResult().getAllErrors().forEach((error) -> {
													    		String fieldName = ((FieldError) error).getField();
													    		String errorMessage = error.getDefaultMessage();
													    		errorMap.put(fieldName, errorMessage);
													    	});
    	return new ErrorResponse(EError.BAD_REQUEST, errorMap);
    }
    
    
	//HTTP 500
    /** Manage bean validation (https://beanvalidation.org/2.0/).
     * 
     * @param excemption
     * @return
     */
	@ApiResponse(responseCode = "500",
				 description = "Generic error.")
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(GenericException.class)
    public ErrorResponse handleGenericExceptions(GenericException e) {

    	log.error("Handling generic exception | {}", e);
    	return new ErrorResponse(e.getError());
    }
    
    
	//HTTP 500
    /** Handling generic exception.
     * 
     * @param excemption
     * @return
     */
	@ApiResponse(responseCode = "500",
				 description = "Internal Server Error.")
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorResponse handleExceptions(Exception e) {
    	
    	log.error("Handling generic Exception | ", e);
    	return new ErrorResponse(EError.GENERIC);
    }
}