package it.notartel.poc.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import it.notartel.poc.model.Glossary;
import it.notartel.poc.model.Glossary.GlossaryItem;
import it.notartel.poc.model.Request;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GlossaryService {
	
	private Glossary glossary = init();
	
	public List<GlossaryItem> getList(Request request) {
		if (request.isWithDescription()) {
			return glossary.getList();
		} else {
			return glossary.getNameList();
		}
	}

	private Glossary init() {
		List<GlossaryItem> list = new ArrayList<>();
		list.add(new GlossaryItem("CNUE", "Consiglio dei Notariati dell'Unione Europea"));
		list.add(new GlossaryItem("CNN", "Consiglio Nazionale del Notariato"));
		list.add(new GlossaryItem("NotBox", "Servizio di hosting cloud dei files (tipo Dropbox)"));
		return new Glossary(list);
	}

}
